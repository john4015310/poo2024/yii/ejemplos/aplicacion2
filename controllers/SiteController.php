<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    

    public function actionEjercicio1(){
        $alumnos=['Mario','Ana','Maheva','Denis'];
        // $alumnos = "hola";
        return $this->render('ejercicio1',['alumnos'=>$alumnos]);
    }
    public function actionEjercicio2a(){
        $fotos = [
            "pic.jpg",
            "foto1.jpg",
            "foto2.jpg",
            "foto3.jpg"
        ];
        return $this->render('ejercicio2a',['imagenes'=>$fotos]);
    }
    public function actionEjercicio2b(){
        $fotos = [
            "pic.jpg",
            "foto1.jpg",
            "foto2.jpg",
            "foto3.jpg"
        ];
        return $this->render('ejercicio2b',['imagenes'=>$fotos]);
    }
    public function actionEjercicio3($id = 0){
        $alumnos = [
        [
            "nombre" => "Mario",
            "poblacion" => "Madrid",
            "imagen" => "pic.jpg"
        ]
        ,
        [
            "nombre" => "Ana",
            "poblacion" => "Barcelona",
            "imagen" => "foto1.jpg"
        ]
        ,
        [
            "nombre" => "Maheva",
            "poblacion" => "Valencia",
            "imagen" => "foto2.jpg"
        ]
        ,
        [
            "nombre" => "Denis",
            "poblacion" => "Sevilla",
            "imagen" => "foto3.jpg"
        ]
        ];
        //le quito la imagen al alumno para pasarlo solo
        $alumnosSinFoto = $alumnos[$id];
        unset($alumnosSinFoto['imagen']);
        return $this->render('ejercicio3',[
            'alumno'=>$alumnos[$id],
            'alumnosSinFoto'=>$alumnosSinFoto
        ]);
    }
    public function actionEjercicio4(){
        $numeros=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
        return $this->render('ejercicio4',[
            'numeros'=>$numeros
        ]);
    }

    public function actionEjercicio5(){
        $datos = [
            [
                'id' => 1,
                'nombre' => 'Mario',
                'poblacion' => 'Madrid',
                'direccion' => 'Calle 1'
            ],
            [
                'id' => 2,
                'nombre' => 'Ana',
                'poblacion' => 'Barcelona',
                'direccion' => 'Calle 2'
            ],
            [
                'id' => 3,
                'nombre' => 'Maheva',
                'poblacion' => 'Valencia',
                'direccion' => 'Calle 3'
            ],
            [
                'id' => 4,
                'nombre' => 'Denis',
                'poblacion' => 'Sevilla',
                'direccion' => 'Calle 4'
            ]
            ];

            //que en una vista se muestre los datos anteriores
            //utilizando cards
            // en el titulo me colocan el id de cada elemento
            //en el texto de la card el nombre
            //el resto de los campos en una lista dentro del card 
            // con el formato asi Nombre del campo valor


        return $this->render('ejercicio5',[
            'datos'=>$datos
        ]);

    }

    public function actionEjercicio6(){
        $datos = [
            [
                'id' => 1,
                'nombre' => 'Mario',
                'poblacion' => 'Madrid',
                'direccion' => 'Calle 1'
            ],
            [
                'id' => 2,
                'nombre' => 'Ana',
                'poblacion' => 'Barcelona',
                'direccion' => 'Calle 2'
            ],
            [
                'id' => 3,
                'nombre' => 'Maheva',
                'poblacion' => 'Valencia',
                'direccion' => 'Calle 3'
            ],
            [
                'id' => 4,
                'nombre' => 'Denis',
                'poblacion' => 'Sevilla',
                'direccion' => 'Calle 4'
            ]
            ];
            //que en una vista se muestre los datos anteriores
            //utilizando cards
            // en el titulo me colocan el id de cada elemento
            //en el texto de la card el nombre
            //el resto de los campos en una lista dentro del card 
            // con el formato asi Nombre del campo valor

            // en este caso utilizar vistas o sub vistas de bootstrap
            //realizar rende dentro de la vista

            return $this->render('ejercicio5',[
                'datos'=>$datos
            ]);
    }
}
