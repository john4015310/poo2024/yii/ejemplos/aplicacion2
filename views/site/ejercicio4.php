<?php

use yii\helpers\Html;

echo $this->render('_listado',[
    'numeros'=>$numeros
]);


?>

<ul class="list-group list-group-horizontal">
    <?php
        foreach($numeros as $numero){
            echo '<li class="list-group-item">'.$numero.'</li>';
        }
    ?>
</ul>

<?php
echo Html::ul($numeros,[
    'class' => 'list-group list-group-horizontal',
    'itemOptions' => [
        'class' => 'list-group-item'
    ]
]);