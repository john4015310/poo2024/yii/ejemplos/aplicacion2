<?php

use yii\helpers\Html;
//ramon rules
echo Html::ul($imagenes,[
    'class' => 'list-unstyled list-group list-group-horizontal',//colocar una clase de bootstrap en la etiqueta ul
    'item'=> function($imagen) {
        $salida = "<li class='list-group-item col-lg-3'>";
        $salida .=  Html::img("@web/imgs/{$imagen}", [
            "class" => " img-thumbnail"
        ]);
        $salida .= "</li>";
        return $salida;
    },
    
]);

