<?php

// sin utilizar Helpers de HTML

use yii\helpers\Html;

// echo "<div class='row'>";
echo Html::beginTag('div', ['class' => 'row']);
foreach ($datos as $dato) {
    echo $this->render("_ejercicio6Item", ['dato' => $dato]);
}

echo Html::endTag('div');
// echo "</div>";
