<?php

use yii\helpers\Html;

?>
<div class="row">

  <?php
  foreach($datos as $dato){
  ?>

  <div class="card m-2" style="width: 18rem;">
    <div class="card-header">
      <h2 class="card-title m-2">Id: <?=$dato["id"]?></h2>
    </div>
    <p class="card-text m-3">Nombre: <?=$dato["nombre"]?></p>
    <ul class="list-group list-group-flush">
      
      <li class="list-group-item">Población : <?=$dato["poblacion"]?></li>
      <li class="list-group-item">Dirección : <?=$dato["direccion"]?></li>
    </ul>
  </div>

  <?php
  }
  ?>

</div>

