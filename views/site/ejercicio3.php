<?php
//sin utilizar helpers de HTML
 use yii\helpers\Html;
?>
<div class="row">
    <div class="col-sm-3">
        <div class="card">
            <img src="<?= Yii::getAlias("@web") ?>/imgs/<?= $alumno["imagen"] ?>">
            <div class="card-body">
                <p>Nombre: <?= $alumno["nombre"] ?></p>
                <p>Poblacion: <?= $alumno["poblacion"] ?></p>

            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="card">
            <img src="<?= Yii::getAlias("@web") ?>/imgs/<?= $alumno["imagen"] ?>">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Nombre: <?= $alumno["nombre"] ?></li>
                <li class="list-group-item">Poblacion: <?= $alumno["poblacion"] ?></li>
            </ul>

        </div>
    </div>
</div>

<!-- <ul class="list-group list-group-horizontal">
    <li class="list-group-item">Nombre: <?=$alumno["nombre"]?></li>
    <li class="list-group-item">Poblacion: <?=$alumno["poblacion"]?></li>
    <li class="list-group-item"><img src="<?=Yii::getAlias('@web')?>/imgs/<?=$alumno["imagen"]?>"
    class="col-3"></li>
</ul>


<div class="card">
    <div class="col-sm-3">
        <div class="card">
            <img src="<?=Yii::getAlias('@web')?>/imgs/<?=$alumno["imagen"]?>">
            <div class="card-body">
            <p>Nombre: <?=$alumno["nombre"]?></p>
            <p >Poblacion: <?=$alumno["poblacion"]?></p>
            </div>
    </div>
</div> -->

<!-- Utilizo helpers  -->

<div class="row">
    <div class="col-sm-3">
        <div class="card">
            <?=Html::img("@web/imgs/". $alumno["imagen"])?>
            <?=Html::ul($alumnosSinFoto,[
                'class' => 'list-group list-group-horizontal',
                'itemOptions' => [
                    'class' => 'list-group-item'
                ]
            ])?>
        </div>
    </div>
</div>



